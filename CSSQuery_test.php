<?php
/**
 *	The CSSQuery object has far too many variations
 *	to test all of them, but here are the most popular
 *
 *    CHANGES - 
 *        08-05-2009 - corrected my horrible typing errors
 */


error_reporting(E_ALL | E_STRICT);

//load the CSSQuery class file
require 'CSSQuery.php';

$br = "\n";//"\n<br>";

$TRUE = 'TRUE';
$FALSE = 'FALSE';

//establish a DOMDocument object
$doc = new DOMDocument();
//load some HTML for testing
$doc->loadHTML('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- xmlns:xsd="uri:sam">-->
						<body>
							<h3>Header 3</h3>
							<table id="dataset">
								<tr>
									<td>column 1</td>
									<td>column 2</td>
									<td>column 3</td>
									<td>column 4</td>
									<td>column 5</td>
								</tr>
								<tr>
									<td>column 1</td>
									<td>column 2</td>
									<td>column 3</td>
									<td>column 4</td>
									<td>column 5</td>
								</tr>
								<tr>
									<td>column 1</td>
									<td>column 2</td>
									<td>column 3</td>
									<td>column 4</td>
									<td>column 5</td>
								</tr>
								<tr>
									<td>column 1</td>
									<td>column 2</td>
									<td>column 3</td>
									<td>column 4</td>
									<td>column 5</td>
								</tr>
								<tr>
									<td>column 1</td>
									<td>column 2</td>
									<td>column 3</td>
									<td>column 4</td>
									<td>column 5</td>
								</tr>
								<tr>
									<td>column 1</td>
									<td>column 2</td>
									<td>column 3</td>
									<td>column 4</td>
									<td>column 5</td>
								</tr>
								<tr>
									<td>column 1</td>
									<td>column 2</td>
									<td>column 3</td>
									<td>column 4</td>
									<td>column 5</td>
								</tr>
								<tr>
									<td>column 1</td>
									<td>column 2</td>
									<td>column 3</td>
									<td>column 4</td>
									<td>column 5</td>
								</tr>
							</table>
							<p class="second-class-name paragraph testing">
								<span id="spanner" class="somewhere">
									<a name="anchor">
										<img src="javascript:;" alt="empty" class="testing"/>
									</a>
								</span><b class="testing">Adjacent siBLing</b><i>General sibling</i>
							</p>
							<p>
								<span>EMpty</span>
								<u>Adjacent sibling</u>
								<b>General sibling</b>
							</p>
							<i> monkey wrench</i>
							<a href="#">Just another test</a>
							<input disabled/>
							<input type="checkbox" checked/>
							<!--<div xsd:testing="test"/>-->
						</body>
					</html>');

//establish new CSSQuery like you would a DOMXPath query
$css = new CSSQuery($doc);

//get elements by tag name
print "{$br}Tag: " . (count( $css->query('p') ) == 2 ? $TRUE : $FALSE);

//get elements by id
print "{$br}ID: " . (count( $css->query('#spanner') ) == 1 ? $TRUE : $FALSE);

//get span elements by id
print "{$br}tag and ID: " . (count( $css->query('span#spanner') ) == 1 ? $TRUE : $FALSE);

//get elements by class name
print "{$br}className: " . (count( $css->query('.paragraph') ) == 1 ? $TRUE : $FALSE);

//get p elements by class name
print "{$br}tag and className: " . (count( $css->query('p.paragraph') ) == 1 ? $TRUE : $FALSE);

//get elements by an attribute
print "{$br}has attribute: " . (count( $css->query('p[class]') ) == 1 ? $TRUE : $FALSE);

//get elements by an attribute
print "{$br}attribute equals: " . (count( $css->query('a[name="anchor"]') ) == 1 ? $TRUE : $FALSE);

//get elements by an attribute
print "{$br}attribute begins with: " . (count( $css->query('a[name^="an"]') ) == 1 ? $TRUE : $FALSE);

//get elements by an attribute
print "{$br}attribute ends with: " . (count( $css->query('img[alt$=ty]') ) == 1 ? $TRUE : $FALSE);

//get elements by an attribute
print "{$br}attribute contains: " . (count( $css->query('a[name*="ch"]') ) == 1 ? $TRUE : $FALSE);

//get elements by an attribute
print "{$br}attribute dashed: " . (count( $css->query('p[class|="second"]') ) == 1 ? $TRUE : $FALSE);

//get elements using descendant selector
print "{$br}descendant: " . (count( $css->query('span img') ) == 1 ? $TRUE : $FALSE);

//get elements using children selector
print "{$br}children: " . (count( $css->query('a > img') ) == 1 ? $TRUE : $FALSE);

//get elements using adjacent sibling selector
print "{$br}adjacent sibling: " . (count( $css->query('span + b') ) == 1 ? $TRUE : $FALSE);

//get elements using general sibling selector
print "{$br}general sibling: " . (count( $css->query('i ~ span') ) == 1 ? $TRUE : $FALSE);

//get elements using first-child psuedo selector
print "{$br}first-child: " . (count( $css->query('p span:first-child') ) == 2 ? $TRUE : $FALSE);

//get elements using last-child psuedo selector
print "{$br}last-child: " . (count( $css->query('p b:last-child') ) == 1 ? $TRUE : $FALSE);

//get elements using only-child psuedo selector
print "{$br}only-child: " . (count( $css->query('a img:only-child') ) == 1 ? $TRUE : $FALSE);

//get elements using disabled psuedo selector
print "{$br}disabled: " . (count( $css->query('input:disabled') ) == 1 ? $TRUE : $FALSE);

//get elements using enabled psuedo selector
print "{$br}enabled: " . (count( $css->query('input:enabled') ) == 1 ? $TRUE : $FALSE);

//get elements using checked psuedo selector
print "{$br}checked: " . (count( $css->query('input:checked') ) == 1 ? $TRUE : $FALSE);

//get elements using checked psuedo selector
print "{$br}empty: " . (count( $css->query('*:empty') ) == 3 ? $TRUE : $FALSE);

//get elements using first-of-type psuedo selector
print "{$br}first-of-type: " . (count( $css->query('tr:first-of-type') ) == 1 ? $TRUE : $FALSE);

//get elements using last-of-type psuedo selector
print "{$br}last-of-type: " . (count( $css->query('td:last-of-type') ) == 8 ? $TRUE : $FALSE);

//get elements using only-of-type psuedo selector
print "{$br}only-of-type: " . (count( $css->query('u:only-of-type') ) == 1 ? $TRUE : $FALSE);

//get elements using not
print "{$br}not: " . (count( $css->query('a:not([href])') ) == 1 ? $TRUE : $FALSE);

//get elements using nth-of-type psuedo selector
print "{$br}nth-of-type: " . (count( $css->query('tr:nth-of-type(odd)') ) == 4 ? $TRUE : $FALSE);

//get elements using nth-last-of-type psuedo selector
print "{$br}nth-last-of-type: " . (count( $css->query('tr:nth-last-of-type(even)') ) == 4 ? $TRUE : $FALSE);

//get elements using nth-child psuedo selector
print "{$br}nth-child: " . (count( $css->query('tr:nth-child(n+2)') ) == 4 ? $TRUE : $FALSE);

//get elements using nth-last-child psuedo selector
print "{$br}nth-last-child: " . (count( $css->query('td:nth-last-child(3)') ) == 8 ? $TRUE : $FALSE);

//get elements using first psuedo selector
print "{$br}first: " . (count( $css->query('tr:first') ) == 1 ? $TRUE : $FALSE);

//get elements using last psuedo selector
print "{$br}last: " . (count( $css->query('tr:last') ) == 1 ? $TRUE : $FALSE);

//get elements using eq psuedo selector
print "{$br}eq: " . (count( $css->query('tr:eq(5)') ) == 1 ? $TRUE : $FALSE);

//get elements using lt psuedo selector
print "{$br}lt: " . (count( $css->query('tr:lt(5)') ) == 5 ? $TRUE : $FALSE);

//get elements using gt psuedo selector
print "{$br}gt: " . (count( $css->query('tr:gt(5)') ) == 2 ? $TRUE : $FALSE);

//get elements using nth psuedo selector
print "{$br}nth: " . (count( $css->query('tr:nth(odd)') ) == 4 ? $TRUE : $FALSE);

//get elements using parent psuedo selector
print "{$br}parent: " . (count( $css->query('a:parent') ) == 1 ? $TRUE : $FALSE);

//get elements using parent-node psuedo selector
print "{$br}parent-node: " . (count( $css->query('a:parent-node') ) == 2 ? $TRUE : $FALSE);

//get elements using parent-node psuedo selector
print "{$br}parent-node(span): " . (count( $css->query('a:parent-node(span)') ) == 1 ? $TRUE : $FALSE);

//get elements using parents psuedo selector
print "{$br}parents: " . (count( $css->query('input:parents') ) == 2 ? $TRUE : $FALSE);

//get elements using parents psuedo selector
print "{$br}parents(p): " . (count( $css->query('span:parents(p)') ) == 2 ? $TRUE : $FALSE);

//get elements using headers psuedo selector
print "{$br}header: " . (count( $css->query(':header') ) == 1 ? $TRUE : $FALSE);

//get elements using checkbox psuedo selector
print "{$br}checkbox: " . (count( $css->query(':checkbox') ) == 1 ? $TRUE : $FALSE);

//get elements using input psuedo selector
print "{$br}input: " . (count( $css->query(':input') ) == 2 ? $TRUE : $FALSE);

//get elements using contains psuedo selector
print "{$br}contains: " . (count( $css->query('b:contains("General")') ) == 1 ? $TRUE : $FALSE);

//get elements using Contains psuedo selector
print "{$br}Contains: " . (count( $css->query('b:Contains(sibling)') ) == 2 ? $TRUE : $FALSE);

//comma
print "{$br}comma: " . (count( $css->query('p, #spanner, tr:first') ) == 4 ? $TRUE : $FALSE);

//$css->registerNamespace('uri:sam', 'xsd');

//print "{$br}ns@attribute: " . (count( $css->query('div[xsd|testing]') ) == 1 ? $TRUE : $FALSE);

CSSQuery::$attributeFilters['>='] = create_function('&$e,&$a,&$v','return $e->hasAttribute($a) && strlen($e->getAttribute($a)) > $v;');

//get elements by an attribute
print "{$br}attribute length > 4: " . (count( $css->query('a[name>=4]') ) == 1 ? $TRUE : $FALSE);

//use # and . as a filter instead of a selector
print "{$br}filter selectors: " . (count( $css->query('span#spanner.somewhere:not([tabindex]), p.paragraph') ) == 2 ? $TRUE : $FALSE);

//use # and . as a filter instead of a selector
print "{$br}filter selectors 2: " . (($x = count( $css->query('span#spanner:not(.somewhere), p.second-class-name:not([tabindex]).paragraph') )) == 1 ? $TRUE : $x);

//use # and . as a filter instead of a selector
print "{$br}filter selectors 3: " . (count( $css->query('.paragraph .testing') ) == 2 ? $TRUE : $FALSE);

$context = $css->query('.paragraph');
//use # and . as a filter instead of a selector
print "{$br}filter selectors 4: " . (count( $css->query('> .testing', $context) ) == 1 ? $TRUE : $FALSE);

?>
