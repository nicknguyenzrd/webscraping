<?php
require_once('vendor/autoload.php');
       // Create the template
       use Httpful\Request;
       $template = Request::init()
           ->method(Http::POST)        // Alternative to Request::post
           ->withoutStrictSsl()        // Ease up on some of the SSL checks
           ->expectsHtml()             // Expect HTML responses
           ->sendsType(Mime::FORM);    // Send application/x-www-form-urlencoded

       // Set it as a template
       Request::ini($template);

       // This new request will have all the settings
       // of our template by default.  We can override
       // any of these settings by settings them on this
       // new instance as we've done with expected type.
       $r = Request::get($uri)->expectsJson();

//Put JSON w/ Basic Auth
       $response = \Httpful\Request::put($uri)                  // Build a PUT request...
       ->sendsJson()                               // tell it we're sending (Content-Type) JSON...
       ->authenticateWith('username', 'password')  // authenticate with basic auth...
       ->body('{"json":"is awesome"}')             // attach a body/payload...
       ->send();   


       //Get and parse JSON
       $uri = "https://www.googleapis.com/freebase/v1/mqlread?query=%7B%22type%22:%22/music/artist%22%2C%22name%22:%22The%20Dead%20Weather%22%2C%22album%22:%5B%5D%7D";
       $response = \Httpful\Request::get($uri)->send();

       echo 'The Dead Weather has ' . count($response->body->result->album) . " albums.\n";



       ?>